import {TestPage} from './test-page';
import {DebugElement} from '@angular/core';
import {HarnessLoader} from '@angular/cdk/testing';
import {MatButtonHarness} from '@angular/material/button/testing';
import {MatInputHarness} from '@angular/material/input/testing';

export class TestMaterialPage extends TestPage {

  harnessLoader: HarnessLoader;
  constructor(_debugElement: DebugElement,
              _harnessLoader: HarnessLoader) {
    super(_debugElement);
    this.harnessLoader = _harnessLoader;
  }
  async getMaterialButton(selector: string) {
    return this.harnessLoader.getHarness(MatButtonHarness.with({selector}))
  }
  async clickMaterialButton(matButtonHarness: MatButtonHarness) {
    return matButtonHarness.click();
  }
  async getMaterialInput(selector: string) {
    return this.harnessLoader.getHarness(MatInputHarness.with({selector}));
  }
  async setMaterialInputValue(matInputHarness: MatInputHarness, value: string) {
    return matInputHarness.setValue(value);
  }
}
