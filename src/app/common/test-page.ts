import {DebugElement} from '@angular/core';
import {By} from '@angular/platform-browser';

export class TestPage {
  constructor(protected debugElement: DebugElement) {
  }
  queryByCss(selector: string)  {
    return this.debugElement.query(By.css(selector));
  }
  queryAllByCss(selector: string) {
    return this.debugElement.queryAll(By.css(selector));
  }
  isExist(selector: string) {
    return this.queryAllByCss(selector).length > 0;
  }
  getTextContent(selector: string) {
    return this.queryByCss(selector).nativeElement.textContent.trim();
  }
}
