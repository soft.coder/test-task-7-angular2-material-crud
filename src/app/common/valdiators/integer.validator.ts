import {AbstractControl, ValidationErrors} from '@angular/forms';

export function integerValidator(control: AbstractControl): ValidationErrors | null {
  const integerRegexp = /^\d*$/;
  const isInteger = integerRegexp.test(control.value);
  return isInteger ? null : {integer: {value: control.value}};
}
export type IntegerError = {value: number};
