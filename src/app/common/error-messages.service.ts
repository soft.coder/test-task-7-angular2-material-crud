import {FormGroup} from '@angular/forms';
import {Injectable} from '@angular/core';
import {IntegerError} from './valdiators/integer.validator';

@Injectable({
  providedIn: 'root'
})
export class ErrorMessagesService
{
  public getErrorMessage(formGroup: FormGroup,  formControlName: string, fieldName?: string | undefined) {
    let formControl = formGroup.get(formControlName);
    if(formControl) {
      if (formControl.dirty && formControl.errors) {
        const errorKey = Object.keys(formControl.errors)[0];
        // @ts-ignore
        if(this[errorKey]) {
          // @ts-ignore
          return this[errorKey](
            fieldName || formControlName,
            formControl.errors[errorKey]);
        }
      }
      return undefined;
    }
    else
      throw Error(`Form control not found`);
  }

  protected required(fieldName: string) {
    return `The ${fieldName} is required`
  }
  protected min(fieldName: string, error: MinError) {
    return `The ${fieldName} must be greater than ${error.min}`
  }
  protected minlength(fieldName: string, error: MinLengthError) {
    return `Minimal length of ${fieldName} is ${error.requiredLength}`
  }
  protected integer(fieldName: string, error: IntegerError) {
    return `The ${fieldName} value must be integer`;
  }
}

type MinError = { min: number, actual: number };
type MinLengthError =  {actualLength: number, requiredLength: number };
