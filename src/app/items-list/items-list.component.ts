import {Component, OnInit} from '@angular/core';
import {Item} from '../item';
import {map, Observable} from 'rxjs';
import {ActivatedRoute} from '@angular/router';
import {ItemApiService} from '../item-api.service';
import {DialogService} from '../common/dialog.service';

@Component({
  selector: 'app-items-list',
  templateUrl: './items-list.component.html',
  styleUrls: ['./items-list.component.scss']
})
export class ItemsListComponent implements OnInit{

  items$!: Observable<Item[]>

  constructor(protected activatedRoute: ActivatedRoute,
              protected itemApiService: ItemApiService,
              protected dialogService: DialogService) {

  }

  ngOnInit() {
    this.items$ = this.activatedRoute.data.pipe(map(data => data['items']));
  }

  deleteItem(item: Item) {
    if(this.dialogService.confirm(`Do you really want delete item '${item.name}' with id:${item.id}`)) {
      this.itemApiService.delete(item.id!).subscribe({
        next: () => {
          this.items$ = this.items$.pipe(map(items => {
            return items.filter(i => item.id !== i.id);
          }));
        },
        error: (error) => {
          let message: string;
          if (error?.body?.error)
            message = `Server error occurred when try delete item '${item.name} with id:${item.id}, error message: ${error.body.error}`;
          else
            message = `Unexpected error occurred when try delete item '${item.name}' with id:${item.id}`;

          this.dialogService.alert(message)
        }
      });
    }
  }
}
