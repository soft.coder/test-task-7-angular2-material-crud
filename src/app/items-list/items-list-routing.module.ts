import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ItemsListComponent } from './items-list.component';
import {ItemApiService} from '../item-api.service';
import {inject} from '@angular/core';

const routes: Routes = [
  {
    path: '',
    component: ItemsListComponent,
    resolve: {
      items: () => inject(ItemApiService).getItems()
    }
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ItemsListRoutingModule { }
