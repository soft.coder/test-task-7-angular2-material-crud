import {ComponentFixture, TestBed} from '@angular/core/testing';

import { ItemsListComponent } from './items-list.component';
import {TestPage} from '../common/test-page';
import {ActivatedRoute, RouterModule} from '@angular/router';
import { Subject} from 'rxjs';
import {Item} from '../item';
import {ItemApiService} from '../item-api.service';
import {Dialog} from '@angular/cdk/dialog';
import {DialogService} from '../common/dialog.service';
import {MatButtonModule} from '@angular/material/button';

describe('ItemsListComponent', () => {
  let component: ItemsListComponent;
  let fixture: ComponentFixture<ItemsListComponent>;
  let activatedRouteSpy: jasmine.SpyObj<ActivatedRoute>;
  let itemApiServiceSpy: jasmine.SpyObj<ItemApiService>;
  let dialogServiceSpy: jasmine.SpyObj<DialogService>;
  let page: ItemsListPage;
  let items = [
    new Item( "item1", "description1", 10, 1),
    new Item( "item2", "description2", 20, 2),
    new Item( "item3", "description3", 30, 3),
    new Item( "item4", "description4", 40, 4),
  ];
  let itemsSubject: Subject<{ items: Item[] }>;

  beforeEach(async () => {
    itemsSubject = new Subject<{items: Item[]}>();
    activatedRouteSpy = jasmine.createSpyObj<ActivatedRoute>(
      {}, {data: itemsSubject});
    itemApiServiceSpy = jasmine.createSpyObj<ItemApiService>(['delete']);
    dialogServiceSpy = jasmine.createSpyObj<DialogService>(['confirm', 'alert']);
    await TestBed.configureTestingModule({
      imports: [RouterModule, MatButtonModule],
      declarations: [ ItemsListComponent ],
      providers: [
        {provide: ActivatedRoute, useValue: activatedRouteSpy},
        {provide: ItemApiService, useValue: itemApiServiceSpy},
        {provide: Dialog, useValue: itemApiServiceSpy}
      ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(ItemsListComponent);
    page = new ItemsListPage(fixture.debugElement);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
  it('should show items', () => {
    itemsSubject.next({items});
    fixture.detectChanges();

    const itemsFragment = page.Items;
    expect(itemsFragment.length).toBe(items.length);
    itemsFragment.forEach(itFr => {
      const item = items.find(i => i.id === itFr.id);
      if(item !== undefined) {
        expect(itFr.id).toBe(item.id ?? NaN);
        expect(itFr.name).toBe(item.name);
        expect(itFr.description).toContain(item.description);
        expect(itFr.cost).toBe(item.cost ?? NaN);
      }
      else fail(`item ID:${itFr.id} Name:${itFr.name} not found`);
    });
  });
  it('should show add item', () => {
    itemsSubject.next({items});
    fixture.detectChanges();

    expect(page.AddItemExist).toBeTrue();
  });
  it('should show no items', () => {
    itemsSubject.next({items: []});
    fixture.detectChanges();

    expect(page.NoItemsExist).toBeTrue();
  })
});


class ItemsListPage extends TestPage {
  protected selectors = {
    items: '.items-list .item:not(.item--add)',
    addItem: '.items-list .item.item--add',
    noItems: '.items-list .items-list__not-items'
  };

  get Items() : ItemFragmentPage[] {
    return this.queryAllByCss(this.selectors.items)
                  .map(i => new ItemFragmentPage(i));
  }
  get AddItemExist() {
    return this.isExist(this.selectors.addItem);
  }
  get NoItemsExist() {
    return this.isExist(this.selectors.noItems);
  }
}

class ItemFragmentPage extends TestPage {
  protected selectors = {
    name: '.item__name',
    id: '.item__id',
    description: '.item__description',
    cost: '.item__cost'
  }
  get name() {
    return this.getTextContent(this.selectors.name);
  }
  get id() {
    return parseInt(this.getTextContent(this.selectors.id).substring(3));
  }
  get description() {
    return this.getTextContent(this.selectors.description);
  }
  get cost() {
    return parseInt(this.getTextContent(this.selectors.cost).substring(1));
  }

}
