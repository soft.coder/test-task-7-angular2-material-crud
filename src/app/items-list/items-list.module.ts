import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ItemsListRoutingModule } from './items-list-routing.module';
import { ItemsListComponent } from './items-list.component';
import { MatButtonModule } from '@angular/material/button';


@NgModule({
  declarations: [
    ItemsListComponent
  ],
  imports: [
    CommonModule,
    MatButtonModule,
    ItemsListRoutingModule
  ]
})
export class ItemsListModule { }
