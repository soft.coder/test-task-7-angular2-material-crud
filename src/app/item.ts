export class Item {

  constructor();
  constructor(name: string, desription: string, cost: number);
  constructor(name: string, description: string, cost: number, id: number);
  constructor(public name: string = '',
              public description: string = '',
              public cost?: number,
              public id?: number) {
  }
}
