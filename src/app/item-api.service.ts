import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {Item} from './item';

@Injectable({
  providedIn: 'root'
})
export class ItemApiService {

  protected apiUrl = 'api/items'
  constructor(protected http: HttpClient){ }

  getItems(): Observable<Item[]> {
    return this.http.get<Item[]>(this.apiUrl);
  }
  get(id: number) {
    return this.http.get<Item>(`${this.apiUrl}/${id}`);
  }
  delete(id: number) {
    return this.http.delete<Item>(`${this.apiUrl}/${id}`);
  }
  update(item: Item) {
    return this.http.put<Item>(`${this.apiUrl}/${item.id}`, item);
  }
  save(item: Item) {
    return this.http.post<Item>(`${this.apiUrl}`, item);
  }
}
