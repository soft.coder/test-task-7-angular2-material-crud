import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import {NotFoundComponent} from './not-found/not-found.component';

const routes: Routes = [
  { path: 'items-list', loadChildren: () => import('./items-list/items-list.module').then(m => m.ItemsListModule) },
  { path: 'item', loadChildren: () => import('./item/item.module').then(m => m.ItemModule) },
  { path: '', pathMatch: 'full', redirectTo: '/items-list'},
  { path: '**', component: NotFoundComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
