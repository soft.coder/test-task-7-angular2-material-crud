import {fakeAsync, TestBed} from '@angular/core/testing';

import { ItemApiService } from './item-api.service';
import {HttpClient} from '@angular/common/http';
import {Item} from './item';
import {of} from 'rxjs';

describe('ItemApiService', () => {
  let service: ItemApiService;
  let httpClientSpy: jasmine.SpyObj<HttpClient> = jasmine.createSpyObj('HttpClient', ['get']);
  let items = [
    new Item( "item1", "description1", 10, 1),
    new Item( "item2", "description2", 20, 2),
    new Item( "item3", "description3", 30, 3),
    new Item( "item4", "description4", 40, 4),
  ]

  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [
        { provide: HttpClient, useValue: httpClientSpy}
      ]
    });
    service = TestBed.inject(ItemApiService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
  it('should get items', fakeAsync(() => {
    let fetchedItems: any;
    httpClientSpy.get.and.returnValue(of(items));
    service.getItems().subscribe(fi => fetchedItems = fi);
    expect(fetchedItems).toBe(items);
  }));
});
