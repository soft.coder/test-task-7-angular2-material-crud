import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ItemRoutingModule } from './item-routing.module';
import { ItemComponent } from './item.component';
import {MatButtonModule} from '@angular/material/button';


@NgModule({
  declarations: [
    ItemComponent
  ],
  imports: [
    CommonModule,
    MatButtonModule,
    ItemRoutingModule
  ]
})
export class ItemModule { }
