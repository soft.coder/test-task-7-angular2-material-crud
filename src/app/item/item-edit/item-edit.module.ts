import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ItemEditRoutingModule } from './item-edit-routing.module';
import { ItemEditComponent } from './item-edit.component';
import {ReactiveFormsModule} from '@angular/forms';
import {MatInputModule} from '@angular/material/input';
import {MatButtonModule} from '@angular/material/button';


@NgModule({
  declarations: [
    ItemEditComponent
  ],
  imports: [
    CommonModule,
    ReactiveFormsModule,
    MatInputModule,
    MatButtonModule,
    ItemEditRoutingModule
  ]
})
export class ItemEditModule { }
