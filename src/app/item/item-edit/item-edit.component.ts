import {Component, OnInit} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {Item} from '../../item';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {integerValidator} from '../../common/valdiators/integer.validator';
import {ErrorMessagesService} from '../../common/error-messages.service';
import {ItemApiService} from '../../item-api.service';

@Component({
  selector: 'app-item-edit',
  templateUrl: './item-edit.component.html',
  styleUrls: ['./item-edit.component.scss']
})
export class ItemEditComponent implements OnInit{


  editItem?: Item;
  itemForm!: FormGroup;
  isPending: boolean = false;
  serverErrorMessage?: string;

  constructor(protected activatedRoute: ActivatedRoute,
              protected router: Router,
              protected fb: FormBuilder,
              protected errorMessagesService: ErrorMessagesService,
              protected itemsApiService: ItemApiService) {
  }

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(data => {
      if(data['item']) {
        this.initItemForm(data['item'])
        this.editItem = data['item'];
      }
      else {
        let emptyItem = new Item();
        this.initItemForm(emptyItem);
      }
    });

  }
  initItemForm(item: Item) {
    this.itemForm = this.fb.group({
      name: [item.name, [Validators.required, Validators.minLength(3)]],
      description: [item.description],
      cost: [item.cost, [Validators.required, integerValidator, Validators.min(1), ]],
    });
  }
  isInvalid(formControlName: string) {
    const formControl = this.itemForm.get(formControlName);
    if(formControl)
      return formControl.invalid;
    else
      throw Error(`Form control with name '${formControlName} not found`);
  }
  getErrorMessage(formControlName: string) {
    return this.errorMessagesService.getErrorMessage(this.itemForm, formControlName);
  }

  onSave() {
    let observer = {
      next: (savedItem: Item) => {
        const id = this.editItem?.id ?? savedItem?.id;
        if(id)
          this.router.navigate(['/item', id]);
        else
          this.router.navigate(['/items-list']);
      },
      error: (error: any) => {
        this.isPending = false;
        this.serverErrorMessage = error.body?.error ?? "Unexpected server error";
      }
    };
    const {name, description, cost} = this.itemForm.value;
    this.isPending = true;
    if(this.editItem && this.editItem.id) {
      const itemForSave = new Item(name, description, parseInt(cost), this.editItem.id);
      this.itemsApiService.update(itemForSave).subscribe(observer);
    }
    else {
      const itemForSave = new Item( name, description, parseInt(cost));
      this.itemsApiService.save(itemForSave).subscribe(observer)
    }
  }

  onCancel() {
    if(this.editItem)
      this.router.navigate(['/item', this.editItem.id]);
    else
      this.router.navigate(['/items-list']);
  }
}
