import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ItemEditComponent } from './item-edit.component';
import {itemResolveFn} from '../item.resolve';

const routes: Routes = [
  {
    path: ':id',
    component: ItemEditComponent,
    resolve: {
      item: itemResolveFn
    }
  },
  {
    path: '',
    component: ItemEditComponent,
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ItemEditRoutingModule { }
