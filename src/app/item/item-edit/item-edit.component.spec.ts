import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ItemEditComponent } from './item-edit.component';
import {TestMaterialPage} from '../../common/test-material-page';
import {MatButtonModule} from '@angular/material/button';
import {ReactiveFormsModule} from '@angular/forms';
import {MatInputModule} from '@angular/material/input';
import {Item} from '../../item';
import {of, Subject} from 'rxjs';
import {ActivatedRoute, Router} from '@angular/router';
import {ItemApiService} from '../../item-api.service';
import {TestbedHarnessEnvironment} from '@angular/cdk/testing/testbed';
import {HarnessLoader} from '@angular/cdk/testing';
import {NoopAnimationsModule} from '@angular/platform-browser/animations';
import {ErrorMessagesService} from '../../common/error-messages.service';

describe('ItemEditComponent', () => {
  let component: ItemEditComponent;
  let fixture: ComponentFixture<ItemEditComponent>;
  let loader: HarnessLoader;
  let itemEditPage: ItemEditPage;
  const item = new Item("item1", "description1", 10, 1);
  type DataType = {item: Item} | {};
  let itemSubject: Subject<DataType>;
  let activatedRouteSpy: jasmine.SpyObj<ActivatedRoute>;
  let itemApiServiceSpy: jasmine.SpyObj<ItemApiService>;
  let routerSpy: jasmine.SpyObj<Router>;
  let errorMessagesSpy: jasmine.SpyObj<ErrorMessagesService>;

  beforeEach(async () => {
    itemSubject = new Subject<DataType>();
    activatedRouteSpy = jasmine.createSpyObj<ActivatedRoute>({},
      {data: itemSubject}
    );
    routerSpy = jasmine.createSpyObj<Router>({
      navigate: new Promise(resolve => resolve(true))
    });
    errorMessagesSpy = jasmine.createSpyObj<ErrorMessagesService>(['getErrorMessage'])
    itemApiServiceSpy = jasmine.createSpyObj<ItemApiService>(['update', 'save']);

    await TestBed.configureTestingModule({
      imports: [
        NoopAnimationsModule,
        ReactiveFormsModule,
        MatInputModule,
        MatButtonModule
      ],
      declarations: [ ItemEditComponent ],
      providers: [
        {provide: ActivatedRoute, useValue: activatedRouteSpy},
        {provide: ItemApiService, useValue: itemApiServiceSpy },
        {provide: Router, useValue: routerSpy}
      ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(ItemEditComponent);
    loader = TestbedHarnessEnvironment.loader(fixture);
    component = fixture.componentInstance;
    itemEditPage = new ItemEditPage(fixture.debugElement, loader);
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
  describe('edit item', () => {
    beforeEach(()=> {
      itemSubject.next({item: item});
      fixture.detectChanges();
    });
    it('should fill input fields', async() => {
      const nameInput = await itemEditPage.getName();
      const descriptionInput = await itemEditPage.getDescription();
      const costInput = await itemEditPage.getCost();

      expect(await nameInput.getValue()).toBe(item.name);
      expect(await descriptionInput.getValue()).toBe(item.description);
      expect(await costInput.getValue()).toBe(item.cost?.toString() ?? '');
    });

    it('should save item', async() => {
      // @ts-ignore
      itemApiServiceSpy.update.and.returnValue(of(null));
      const newName = '123';

      await itemEditPage.setName(newName);
      await itemEditPage.clickSaveBtn();

      let itemForUpdate = new Item(newName, item.description, item.cost!, item.id!);
      expect(itemApiServiceSpy.update).toHaveBeenCalledWith(itemForUpdate);
      expect(routerSpy.navigate).toHaveBeenCalledWith(['/item', item.id]);
    });

    it('cancel button must redirect to item view', async () => {
      await itemEditPage.clickCancelBtn();

      expect(routerSpy.navigate).toHaveBeenCalledWith(['/item', item.id]);
    });

  });
  describe('add item', () => {
    beforeEach(()=> {
      itemSubject.next({});
      fixture.detectChanges();
    });
    it('should have empty input fields', async() => {
      const nameInput = await itemEditPage.getName();
      const descriptionInput = await itemEditPage.getDescription();
      const costInput = await itemEditPage.getCost();

      expect(await nameInput.getValue()).toBe('');
      expect(await descriptionInput.getValue()).toBe('');
      expect(await costInput.getValue()).toBe('');
    });

    it('should add item', async() => {
      const id = 1;
      const name = 'Item100';
      const description = 'Description';
      const cost = 123;
      const itemServerReturn = new Item(name, description, cost, id);
      itemApiServiceSpy.save.and.returnValue(of(itemServerReturn));

      await itemEditPage.setName(name)
      await itemEditPage.setDescription(description);
      await itemEditPage.setCost(cost.toString());
      await itemEditPage.clickSaveBtn();

      expect(itemApiServiceSpy.save).toHaveBeenCalled();
      const itemForSave = new Item(name, description, cost);
      expect(itemApiServiceSpy.save.calls.mostRecent().args[0]).toEqual(itemForSave)
      expect(routerSpy.navigate).toHaveBeenCalledWith(['/item', id]);
    });

    it('cancel button must redirect to item list', async () => {
      await itemEditPage.clickCancelBtn();

      expect(routerSpy.navigate).toHaveBeenCalledWith(['/items-list']);
    });
  });



});

class ItemEditPage extends TestMaterialPage {
  async getName() {
    return this.getMaterialInput('.item-edit__name input');
  }
  async setName(value: string) {
    return this.setMaterialInputValue(await this.getName(), value);
  }
  async getDescription() {
    return this.getMaterialInput('.item-edit__description textarea');
  }
  async setDescription(value: string) {
    return this.setMaterialInputValue(await this.getDescription(), value);
  }
  async getCost() {
    return this.getMaterialInput('.item-edit__cost input');
  }
  async setCost(value: string) {
    return this.setMaterialInputValue(await this.getCost(), value);
  }
  async getSaveBtn() {
    return this.getMaterialButton('.item-edit__save');
  }
  async clickSaveBtn() {
    return this.clickMaterialButton(await this.getSaveBtn());
  }
  async getCancelBtn() {
    return this.getMaterialButton('.item-edit__cancel');
  }
  async clickCancelBtn() {
    return this.clickMaterialButton(await this.getCancelBtn());
  }
}

