import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import { ItemComponent } from './item.component';
import {NotFoundComponent} from '../not-found/not-found.component';
import {itemResolveFn} from './item.resolve';

const routes: Routes = [
  { path: 'add', loadChildren: () => import('./item-edit/item-edit.module').then(m => m.ItemEditModule) },
  { path: 'edit', loadChildren: () => import('./item-edit/item-edit.module').then(m => m.ItemEditModule)},
  {
    path: ':id',
    pathMatch: 'full',
    component: ItemComponent,
    resolve: {
      item: itemResolveFn,
    }
  },
  { path: '**', component: NotFoundComponent }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ItemRoutingModule { }
