import {ResolveFn} from '@angular/router';
import {inject} from '@angular/core';
import {ItemApiService} from '../item-api.service';
import {catchError, of, tap, throwError} from 'rxjs';
import {Item} from '../item';

export const itemResolveFn : ResolveFn<Item | null> = (route)  => {
  let id: number;
  if(!isNaN(id = parseInt(route.paramMap.get("id")!)))
    return inject(ItemApiService).get(id).pipe(
      tap(() => {}),
      catchError(() => of(null))
    );
  else
    return throwError(() => new Error(`route parameter id is NaN, id:${id}`));
}
