import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ItemComponent } from './item.component';
import {ActivatedRoute} from '@angular/router';
import {Subject} from 'rxjs';
import {Item} from '../item';

describe('ItemComponent', () => {
  let component: ItemComponent;
  let fixture: ComponentFixture<ItemComponent>;
  let activatedRouteSpy: jasmine.SpyObj<ActivatedRoute>;
  let itemSubject: Subject<{ item: Item }>;

  beforeEach(async () => {
    itemSubject = new Subject<{item: Item}>();
    activatedRouteSpy = jasmine.createSpyObj<ActivatedRoute>(
      {}, {data: itemSubject});

    await TestBed.configureTestingModule({
      declarations: [ ItemComponent ],
      providers: [
        {provide: ActivatedRoute, useValue: activatedRouteSpy}
      ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(ItemComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
